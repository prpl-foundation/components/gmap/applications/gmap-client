# gmap-client

## Starting gmap modules
![relation to other components](./diagrams/gmap-client-highlevel.drawio.svg)

The responsibility of gmap-client is to start the gmap modules such as
[gmap-mod-self](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-mod-self/),
[gmap-mod-ethernet-dev](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-mod-ethernet-dev/),
[gmap-mod-upnp](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-mod-upnp/), etc.

There are two ways these gmap-client can start these gmap modules:
- If the config option `CONFIG_SAH_AMX_GMAP_CLIENT_ONE_PROCESS` is set, then the gmap modules are loaded as .so files into the gmap-client process.
- If that option is not set, then each gmap module is run as its own process.

Running in one process does not reduce communication overhead since modules never communicate directly and instead communicate with gmap-server which still runs as a separate process. Furthermore, running in one process decreases stability since a crash in one module causes all other modules to stop as well. Therefore, it is recommended to keep `CONFIG_SAH_AMX_GMAP_CLIENT_ONE_PROCESS` disabled.

## Finding gmap modules
In order to start a gmap module, gmap-client must be able to know where to find modules.

The init script of gmap-client looks for .odl files in `/etc/amx/gmap-client/modules/`. Each .odl file is one module.

Gmap modules have to install their .odl file in this location in order to be started at boot. Gmap modules must not have their own `/etc/init.d/` entry.

## Starting and stopping an individual gmap module

For debugging and investigation purposes, one sometimes wants to start and stop individual modules. For example, when two modules both change the same gmap device.

To stop an individual gmap module, first disable amx-processmonitor since it can restart processes automatically.
```console
root@prplOS:~# /etc/init.d/amx-processmonitor stop
```
Next, the individual process of the gmap module can be stopped as follows (this is not possible if it is [configured that all gmap modules run in one process](#starting-gmap-modules)).
```console
root@prplOS:~# killall gmap-mod-ethernet-dev
```

To start one gmap module again, first check if the module is not already running because running a module twice should be avoided, and then start the module like so:
```console
root@prplOS:~# ps ax | grep gmap-mod-ethernet-dev | grep -v grep
root@prplOS:~# gmap-mod-ethernet-dev -D /etc/amx/gmap-client/modules/gmap-mod-ethernet-dev.odl
root@prplOS:~# 
```
