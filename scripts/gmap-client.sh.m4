#!/bin/sh
name="gmap-client"
modules_odl="/etc/amx/$name/gmap-modules.odl"
case $1 in
    boot | start) 
        rm ${modules_odl}
        for module in $(find /usr/lib/amx/${name}/modules -name "*.so" | sort); do
            filename=$(basename -- "${module}")
            filename="${filename%.*}"
            echo -e "#include \"modules/${filename}.odl\";">>${modules_odl}    
        done
        ${name} -D
        ;;
    shutdown | stop) 
        [ -f /var/run/${name}.pid ] && kill $(cat /var/run/${name}.pid)
        ;;
    restart)
        $0 stop
        sleep 1
        $0 start
        ;;
esac
