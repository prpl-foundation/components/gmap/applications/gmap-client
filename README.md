 gMap Client 

[[_TOC_]]

## Introduction

The gmap-client acts as a process container or starter.

Depending on the configuration all gMap Client modules will be loaded and executed in the gmap-client process or will be run as seperated processes.
Running the gMap client modules in the gmap-client process instead of in the gmap-server will prevent potential deadlocks caused by communication loops.

See [./doc/README.md](doc/README.md) for more information and diagrams.