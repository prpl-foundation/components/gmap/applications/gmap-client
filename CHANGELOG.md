# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.3.1 - 2024-04-10(10:09:05 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.3.0 - 2024-03-26(11:18:44 +0000)

### Other

- Write gmap-client documentation

## Release v1.2.1 - 2023-11-19(12:32:50 +0000)

### Other

- create a restart target for amx process monitor

## Release v1.2.0 - 2023-11-03(12:25:04 +0000)

### Other

- Use separate process per module by default

## Release v1.1.2 - 2023-10-23(09:28:57 +0000)

### Other

- [LCM] add all feed wifi services components to the wifion lcm app

## Release v1.1.1 - 2023-03-02(09:08:29 +0000)

### Other

- [amx][gmap-client] /etc/init.d/gmap-client does nothing on non-internal build

## Release v1.1.0 - 2023-01-05(11:54:40 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v1.0.3 - 2022-09-15(13:27:25 +0000)

### Other

- Fix `/etc/init.d/gmap-client stop` not working

## Release v1.0.2 - 2022-08-30(10:29:11 +0000)

### Other

- Gmap: Fix default and make gmap default startup priority configurable

## Release v1.0.1 - 2022-07-27(09:18:51 +0000)

### Other

- Opensource component

## Release v1.0.0 - 2022-07-26(08:39:26 +0000)

### Other

- [prpl][gMap] gMap needs to be split in a gMap-core and gMap-client process

